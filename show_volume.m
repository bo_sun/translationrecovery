clear, clc, close all;
x_dimension = 750;
y_dimension = 517;
z_dimension = 194;

file1 = fopen ('./build/object_volume_xy.txt', 'r');
object_volume_xy = fscanf (file1, '%f');

file2 = fopen ('./build/scene_volume_xy.txt','r');
scene_volume_xy = fscanf (file2, '%f');

file3 = fopen ('./build/object_volume_z.txt', 'r');
object_volume_z = fscanf (file3, '%f');

file4 = fopen ('./build/scene_volume_z.txt','r');
scene_volume_z  = fscanf (file4, '%f');

object_xy = reshape(object_volume_xy, [y_dimension, x_dimension]);
scene_xy = reshape(scene_volume_xy, [y_dimension,x_dimension]);

figure, imshow(object_xy, [])
figure, imshow(scene_xy,[])

x = 1: z_dimension;

figure, plot (x, object_volume_z, 'g', x, scene_volume_z, 'r')