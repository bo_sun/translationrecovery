/*********************************************
 * Author: Bo Sun                            *
 * Afflication: TAMS, University of Hamburg  *
 * E-Mail: bosun@informatik.uni-hamburg.de   *
 *         user_mail@QQ.com                  *
 * Date: April 23, 2015                       *
 * Licensing: GNU GPL license.               *
 *********************************************/
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/console/print.h>
#include <pcl/console/parse.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/filter.h>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>

#include <cmath>
#include <algorithm>
#include <vector>
#include <eigen3/Eigen/Dense>

// For debug
#include <time.h>
#include <fstream>

#include "TestTranslationRecovery.h"
#include "TestTranslationRecovery.hpp"

#ifndef TYPE_DEFINITION_
#define TYPE_DEFINITION_

// Types
typedef pcl::PointNormal PointNT;
typedef pcl::PointCloud<PointNT> PointCloudNT;
typedef pcl::visualization::PointCloudColorHandlerCustom<PointNT> ColorHandlerT;
typedef Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrixRowXf;
#endif /*TYPE_DEFINITION_*/

Eigen::Vector3f voxel_size;

int
main (int argc, char**argv)
{
    // Point clouds
    PointCloudNT::Ptr object(new PointCloudNT);
    PointCloudNT::Ptr scene(new PointCloudNT);
    PointCloudNT::Ptr object_translated (new PointCloudNT);

    // Load object and scene point clouds
    pcl::console::print_highlight("Loading point clouds...\n");
    pcl::console::print_info(">>>Ignore the below warning please<<<\n");
    pcl::console::print_info("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n");
    if (pcl::io::loadPCDFile<PointNT>(argv[1], *object)<0)
    {
        pcl::console::print_error("Error loading object file!\n");
        return (1);
    }
    pcl::console::print_info("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\n");
    pcl::console::print_info(">>>Ignore the above warning please<<<\n");

    // Simulate the scene
    voxel_size << 0.1, 0.1, 0.1;

    int offset_x = 50, offset_y = 25, offset_z = 5;

    Eigen::Affine3f transform = Eigen::Affine3f::Identity();
    transform.translation() << offset_x*voxel_size(0),
            offset_y*voxel_size(1),
            offset_z*voxel_size(2);

    printf ("Rotation matrix :\n");
    printf ("    | %6.3f %6.3f %6.3f | \n", transform (0, 0), transform (0, 1), transform (0, 2));
    printf ("R = | %6.3f %6.3f %6.3f | \n", transform (1, 0), transform (1, 1), transform (1, 2));
    printf ("    | %6.3f %6.3f %6.3f | \n", transform (2, 0), transform (2, 1), transform (2, 2));
    printf ("Translation vector :\n");
    printf ("t = < %6.3f, %6.3f, %6.3f >\n\n", transform (0, 3), transform (1, 3), transform (2, 3));

    pcl::transformPointCloudWithNormals(*object, *scene, transform);

    pcl::visualization::PCLVisualizer visu("Translated");
    visu.addPointCloud(object, ColorHandlerT (object, 255.0,255.0, 255.0), "object");
    visu.addPointCloud(scene, ColorHandlerT(scene, 0.0, 0.0, 255.0), "scene");
    visu.spin();

    offset_x = 0;
    offset_y = 0;
    offset_z = 0;
    // Remove the NaN points in object and scene if any
    pcl::console::print_highlight("Remove the NaN points if any...\n");
    std::vector<int> indices_object_nan, indices_scene_nan;
    pcl::removeNaNFromPointCloud(*object, *object, indices_object_nan);
    pcl::removeNaNFromPointCloud(*scene, *scene, indices_scene_nan);

    // compute the range of the point clouds
    PointNT object_minpt, object_maxpt;
    PointNT scene_minpt, scene_maxpt;
    pcl::getMinMax3D<PointNT>(*object, object_minpt, object_maxpt);
    pcl::getMinMax3D<PointNT>(*scene,  scene_minpt,  scene_maxpt);

    // determine the range of the volume
    Eigen::Vector3f volume_minpt;
    Eigen::Vector3f volume_maxpt;
    volume_minpt << std::min(object_minpt.x, scene_minpt.x) - 2.0,
            std::min(object_minpt.y, scene_minpt.y) - 2.0,
            std::min(object_minpt.z, scene_minpt.z) - 2.0;
    volume_maxpt << std::max(object_maxpt.x, scene_maxpt.x) +2.0,
            std::max(object_maxpt.y, scene_maxpt.y) +2.0,
            std::max(object_maxpt.z, scene_maxpt.z) +2.0;

    // determine the size of the volume
    Eigen::Vector3i volumesize;
    volumesize(0) = ceil((volume_maxpt(0)-volume_minpt(0))/voxel_size(0))+1;
    volumesize(1) = ceil((volume_maxpt(1)-volume_minpt(1))/voxel_size(1))+1;
    volumesize(2) = ceil((volume_maxpt(2)-volume_minpt(2))/voxel_size(2))+1;

    pcl::console::print_info("The size of the volume: x %i, y %i, z %i \n",
                             volumesize(0), volumesize(1), volumesize(2));

    // convert point cloud to volume
    EigenMatrixRowXf object_xy = EigenMatrixRowXf::Zero (volumesize(0), volumesize(1));
    EigenMatrixRowXf scene_xy  = EigenMatrixRowXf::Zero (volumesize(0), volumesize(1));
    double *object_z = (double*) calloc (volumesize(2), sizeof(double));
    double *scene_z  = (double*) calloc (volumesize(2), sizeof(double));

    point2volume(*object, voxel_size, volume_minpt, volume_maxpt,
                 object_xy, object_z);
    point2volume(*scene, voxel_size, volume_minpt, volume_maxpt,
                 scene_xy, scene_z);

    ofstream file;
    file.open("object_volume_xy.txt");
    for (int i= 0; i<object_xy.size(); i++)
    {
        file << *(object_xy.data()+i) << std::endl;
    }
    file.close();

    ofstream file1;
    file1.open("scene_volume_xy.txt");
    for (int i= 0; i<scene_xy.size(); i++)
    {
        file1 << *(scene_xy.data()+i) << std::endl;
    }
    file1.close();

    ofstream file2;
    file2.open("object_volume_z.txt");
    for (int i= 0; i<volumesize(2); i++)
    {
        file2 << *(object_z+i) << std::endl;
    }
    file2.close();

    ofstream file3;
    file3.open("scene_volume_z.txt");
    for (int i= 0; i<volumesize(2); i++)
    {
        file3 << *(scene_z+i) << std::endl;
    }

    file3.close();

    PhaseCorrelation2D(scene_xy, object_xy,
                       volumesize(0), volumesize(1),
                       offset_x, offset_y);
    PhaseCorrelation1D(scene_z, object_z,
                       volumesize(2), offset_z);

    pcl::console::print_info("The offset_x is %i. \n", offset_x);
    pcl::console::print_info("The offset_y is %i. \n", offset_y);
    pcl::console::print_info("The offset_z is %i. \n", offset_z);

    Eigen::Affine3f transform1 = Eigen::Affine3f::Identity();

    transform1.translation() << offset_x*voxel_size(0),
            offset_y*voxel_size(1),
            offset_z*voxel_size(2);

    pcl::transformPointCloudWithNormals(*object,*object_translated, transform1);

    // show final result
    pcl::visualization::PCLVisualizer visual("Final");
    visual.addPointCloud (object_translated, ColorHandlerT (object_translated, 0.0, 255.0, 0.0), "object_translated");
    visual.addPointCloud (scene, ColorHandlerT (scene, 0.0, 0.0, 255.0), "scene");
    visual.spin();

    return (0);
}

